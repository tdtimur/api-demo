from fastapi import status, Security, HTTPException
from fastapi.security import APIKeyHeader, APIKeyQuery

QUERY = APIKeyQuery(name="api-key", auto_error=False)
HEADER = APIKeyHeader(name="x-api-key", auto_error=False)

VALID_KEYS = ['abcd']


def api_key(
    api_key_query: str = Security(QUERY),
    api_key_header: str = Security(HEADER),
) -> str:
    if api_key_query in VALID_KEYS:
        return api_key_query
    if api_key_header in VALID_KEYS:
        return api_key_header
    raise HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Invalid or missing API Key",
    )

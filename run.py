import uvicorn

if __name__ == "__main__":
    uvicorn.run(
        "main:app",
        loop="uvloop",
        use_colors=True,
    )

from fastapi import FastAPI, Security
from random import randint
from security import api_key

app = FastAPI()


@app.get("/")
async def root():
    random_number = randint(0, 100)
    return {"message": f"Hello, World #{random_number}!"}


@app.get("/health")
async def health(key: str = Security(api_key)):
    return {"message": "OK"}
